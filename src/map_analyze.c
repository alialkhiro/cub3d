/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_analyze.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/28 11:26:59 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/07/05 10:14:52 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

int	error_map(char *err_msg, int line)
{
	ft_putstr_fd(err_msg, 2);
	ft_putnbr_fd(line, 2);
	ft_putstr_fd("\n", 2);
	return (1);
}

int	map_verfy(t_map *map, int line, int i)
{
	if (map->map[line][i] == '0' && (i == 0
		|| map->map == &map->map[line]
		|| !map->map[line]
		|| map->map[line][i + 1] == '\0'
		|| ft_strlen(map->map[line - 1]) < (size_t)(i + 1)
		|| ft_strlen(map->map[line + 1]) < (size_t)(i + 1)
		|| map->map[line][i + 1] == ' '
		|| map->map[line][i - 1] == ' '
		|| map->map[line - 1][i + 1] == ' '
		|| map->map[line - 1][i - 1] == ' '
		|| map->map[line - 1][i] == ' '
		|| map->map[line + 1][i + 1] == ' '
		|| map->map[line + 1][i - 1] == ' '
		|| map->map[line + 1][i] == ' '))
		return (1);
	return (0);
}

int	layout_analyze(t_map *map, int line)
{
	int	i;
	int	j;

	j = 0;
	map->map = &map->data[line];
	while (map->map[j])
	{
		i = 0;
		if (!map->map[j][0])
			return (error_map("Empty line in map at line ", line + j + 1));
		while (map->map[j][i])
		{
			if (!ft_strchr("01NSWE ", map->map[j][i]))
				return (error_map("Invalid character in map at line ",
						line + j + 1));
			if (map_verfy(map, j, i))
				return (error_map("Map not closed around line ", line + j + 1));
			i++;
		}
		j++;
	}
	return (0);
}

int	get_player(t_map *map)
{
	int	i;
	int	j;

	i = 0;
	while (map->map[i])
	{
		j = 0;
		while (map->map[i][j])
		{
			if (ft_strchr("NSWE", map->map[i][j]))
			{
				if (map->player_x != -1)
					return (1);
				map->player_dir = map->map[i][j];
				map->player_x = i;
				map->player_y = j;
			}
			j++;
		}
		i++;
	}
	return (0);
}

int	map_analyze(t_map *map)
{
	int	line;

	line = 0;
	while (map->data[line][0] == '\0')
		line++;
	if (texture_analyze(map, &line))
		error_handler("", map, map_clear);
	while (map->data[line][0] == '\0')
		line++;
	if (layout_analyze(map, line))
		error_handler("", map, map_clear);
	if (get_player(map))
		error_handler("Multiple player starting points\n", map, map_clear);
	return (0);
}
