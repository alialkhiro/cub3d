/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_reader.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/11 10:36:57 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/06/28 13:53:34 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

/*clear the reader object and print an error message*/
void	reader_clear(void *arg)
{
	t_reader	*reader;

	reader = (t_reader *)arg;
	if (reader->buffer)
		free(reader->buffer);
	if (reader->data)
		free(reader->data);
	if (reader->fd > -1)
		close(reader->fd);
	free(reader);
}

/*initialize the reader object and malloc its buffer*/
void	reader_init(t_reader *reader)
{
	reader->read_count = 1;
	reader->fd = -1;
	reader->data = malloc(sizeof (*(reader->data)));
	if (!reader->data)
		error_handler("Memory error.\n", NULL, NULL);
	reader->data[0] = '\0';
	reader->buffer = malloc(BUFF_SIZE * sizeof (*(reader->buffer)) + 1);
	if (!reader->buffer)
		error_handler("Memory error.\n", (void *)reader, reader_clear);
}

// Returns file discriptor on success, -1 on fail.
int	get_file(char *file)
{
	int	fd;

	fd = open(file, O_RDONLY);
	if (fd < 0)
	{
		perror(file);
		return (-1);
	}
	return (fd);
}

/*reads all the data in fd and returns each line in a string in a null
 terminated array*/
char	*ft_read_file(char *file)
{
	t_reader	reader;

	reader_init(&reader);
	reader.fd = get_file(file);
	if (reader.fd < 3)
		error_handler("", &reader, reader_clear);
	while (reader.read_count)
	{
		reader.read_count = read(reader.fd, reader.buffer, BUFF_SIZE + 1);
		if (reader.read_count < 0)
			error_handler("File reading Error\n", &reader, reader_clear);
		reader.buffer[reader.read_count] = '\0';
		reader.temp = ft_strjoin(reader.data, reader.buffer);
		if (!reader.temp)
			error_handler("Memory Error\n", &reader, reader_clear);
		free(reader.data);
		reader.data = reader.temp;
	}
	free(reader.buffer);
	close(reader.fd);
	return (reader.data);
}
