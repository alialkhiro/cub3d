/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture_analyze.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/05 09:35:29 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/07/05 10:13:54 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

int	identify(char *line)
{
	int	id;

	id = -1;
	if (line[0] == 'N' && line[1] == 'O')
		id = 0;
	if (line[0] == 'E' && line[1] == 'A')
		id = 1;
	if (line[0] == 'S' && line[1] == 'O')
		id = 2;
	if (line[0] == 'W' && line[1] == 'E')
		id = 3;
	if (line[0] == 'F')
		id = 4;
	if (line[0] == 'C')
		id = 5;
	return (id);
}

int	parse_color(t_map *map, char *info, int id)
{
	int		*color;
	char	*ptr;

	color = &(map->ceiling[0]);
	if (id == 5)
		color = &(map->floor[0]);
	color[0] = ft_atoi(&info[1]);
	if (color[0] > 255 || color[0] < 0)
		return (1);
	ptr = ft_strchr(&info[1], ',');
	if (ptr == 0)
		return (1);
	color[1] = ft_atoi(ptr + 1);
	if (color[1] > 255 || color[1] < 0)
		return (1);
	ptr = ft_strchr(ptr + 1, ',');
	if (ptr == 0)
		return (1);
	color[2] = ft_atoi(ptr + 1);
	if (color[2] > 255 || color[2] < 0)
		return (1);
	return (0);
}

int	set_identifier(t_map *map, char *info, int id)
{
	clear_space(&info);
	if (id > 3)
	{
		if (parse_color(map, info, id))
			return (1);
	}
	else
	{
		if (id == 0)
			map->north = &(info[2]);
		if (id == 1)
			map->east = &(info[2]);
		if (id == 2)
			map->south = &(info[2]);
		if (id == 3)
			map->west = &(info[2]);
	}
	return (0);
}

int	all_flags(int flags[6])
{
	int	i;

	i = 0;
	while (i < 6)
	{
		if (flags[i] != 1)
			return (0);
		i++;
	}
	return (1);
}

int	texture_analyze(t_map *map, int *line)
{
	int	flags[6];
	int	id;

	ft_bzero((void *)flags, 6 * sizeof(int));
	while (!all_flags(flags))
	{
		id = identify(map->data[*line]);
		if (id < 0)
			return (error_map("invalid identifier on line ", *line + 1));
		if (flags[id] == 1)
			return (error_map("doublicated identifier on line ", *line + 1));
		if (set_identifier(map, map->data[*line], id))
			return (error_map("Invalid color value at line ", *line + 1));
		else
			flags[id] = 1;
		*line = *line + 1;
		while (map->data[*line][0] == '\0')
			*line = *line + 1;
	}
	return (0);
}
