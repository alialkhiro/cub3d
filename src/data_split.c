/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_split.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/05 09:31:13 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/07/05 09:34:02 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

// Test input and if file name has correct extension.
// Returns 0 on success and 1 on fail.
void	check_input(char **argv, int argc)
{
	int	fd;

	if (argc != 2)
		error_handler("Use: ./cub3D <map_name>.cub\n", NULL, NULL);
	if (ft_strlen(argv[1]) < 5 || argv[1][(int)ft_strlen(argv[1]) - 5] == 47)
		error_handler("Error: invalid file name\n", NULL, NULL);
	if (ft_strncmp(".cub", &argv[1][(int)ft_strlen(argv[1]) - 4], 4))
		error_handler("Error: File etention must be .cub\n", NULL, NULL);
	fd = open(argv[1], O_DIRECTORY);
	if (fd >= 0)
	{
		close(fd);
		error_handler("Error: file is a directory\n", NULL, NULL);
	}
}

/*counts how many instances of c exist in str*/
int	char_count(char *str, char c)
{
	int		count;

	count = 0;
	while (*str)
	{
		if (*str == c)
			count++;
		str++;
	}
	return (count);
}

/*returns a null terminated array of pointers to the start of each line in data
and replacing the new line character \n with EOF \0 . */
char	**data_split(char *data, char splitter)
{
	char	**temp;
	int		i;

	i = 0;
	temp = malloc((char_count(data, '\n') + 2) * sizeof(*temp));
	if (!temp)
		return (NULL);
	while (*data)
	{
		temp[i] = data;
		while (*data != '\0' && *data != splitter)
			data++;
		if (*data == splitter)
		{
			*data = '\0';
			data++;
		}
		i++;
	}
	temp[i] = NULL;
	return (temp);
}
