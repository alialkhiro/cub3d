/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_loader.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/28 11:40:48 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/07/05 10:10:36 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

void	error_handler(char *err_msg, void *arg, void (*func)(void *))
{
	if (func)
		func(arg);
	ft_putstr_fd(err_msg, 2);
	exit(1);
}

void	map_clear(void *arg)
{
	t_map	*map;

	map = (t_map *)arg;
	if (map)
	{
		if (map->data)
		{
			free(map->data[0]);
			free(map->data);
		}
		free(map);
	}
}

t_map	*map_init(char *data)
{
	t_map	*map;

	map = malloc(sizeof(*map));
	if (!map)
		return (NULL);
	map->data = data_split(data, '\n');
	map->map = NULL;
	if (!map->data)
		error_handler("Memory error\n", map, map_clear);
	map->player_x = -1;
	map->player_y = -1;
	return (map);
}

t_map	*map_loader(char *file)
{
	char	*data;
	t_map	*map;

	data = ft_read_file(file);
	printf("File reading done\n");
	map = map_init(data);
	printf("Map init is done\n");
	map_analyze(map);
	return (map);
}

void	map_print(t_map *map)
{
	while (*(map->map))
	{
		printf("%s\n", *(map->map));
		map->map++;
	}
	printf("Texture 1 %s\n", map->north);
	printf("Texture 2 %s\n", map->south);
	printf("Texture 3 %s\n", map->east);
	printf("Texture 4 %s\n", map->west);
	printf("Player is at (%d, %d) directed towards %c\n", map->player_x, map->player_y, map->player_dir);
	printf("Floor color is %d, %d, %d\n", map->floor[0], map->floor[1], map->floor[2]);
	printf("Cealing color is %d, %d, %d\n", map->ceiling[0], map->ceiling[1], map->ceiling[2]);
}

int	main(int argc, char **argv)
{
	t_map	*map;

	check_input(argv, argc);
	map = map_loader(argv[1]);
	map_print(map);
	map_clear(map);
	return (0);
}
