/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/28 06:55:30 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/03/17 13:46:32 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	*ft_gnl_memmove(char *d, char *s, size_t n)
{
	if (s == d)
		return (d);
	if (s < d)
	{
		while (n--)
			*(d + n) = *(s + n);
		return (d);
	}
	while (n--)
		*(d++) = *(s++);
	return (d);
}

char	*ft_gnl_strjoin(char *s1, char *s2)
{
	size_t	i;
	size_t	size1;
	size_t	size2;
	char	*dest;

	i = -1;
	size1 = ft_gnl_strlen(s1);
	size2 = ft_gnl_strlen(s2);
	dest = (char *)malloc(size1 + size2 + 1);
	if (dest)
	{
		while (++i < size1 && size1 != 0)
			dest[i] = s1[i];
		while (i <= size1 + size2)
		{
			dest[i] = s2[i - size1];
			i++;
		}
	}
	else
		write(2, "GNL Memory error\n", 17);
	free(s1);
	return (dest);
}

char	*ft_gnl_substr(char *s, size_t start, size_t len)
{
	size_t	i;
	char	*sbstr;

	if (!s)
		return ("");
	if ((size_t)start > ft_gnl_strlen(s))
		return ("");
	if (ft_gnl_strlen(s) - start < len)
		len = ft_gnl_strlen(s) - start;
	sbstr = (char *)malloc(sizeof(char) * (len + 1));
	if (!sbstr)
	{
		write(2, "GNL Memory error\n", 17);
		return (0);
	}
	i = 0;
	while (i < len && s[start + i])
	{
		sbstr[i] = *(s + start + i);
		i++;
	}
	sbstr[i] = '\0';
	return (sbstr);
}

size_t	findnl(char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '\n')
			return (i);
		i++;
	}
	return (i);
}

size_t	ft_gnl_strlen(char *str)
{
	size_t	i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}
