/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/03 16:41:38 by algeorge          #+#    #+#             */
/*   Updated: 2023/03/14 16:07:42 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strs_sort(char **strs, int strs_len)
{
	int		i;
	int		j;
	char	*temp;

	i = 0;
	if (strs_len == 1)
		return (strs);
	while (i < strs_len)
	{
		j = i + 1;
		while (j < strs_len)
		{
			if (ft_strcmp(strs[i], strs[j]) > 0)
			{
				temp = strs[i];
				strs[i] = strs[j];
				strs[j] = temp;
			}
			j++;
		}
		i++;
	}
	return (strs);
}

char	**ft_free_split(char **strs, int freeptr)
{
	int	i;

	i = 0;
	if (strs)
	{
		while (strs[i])
		{
			free(strs[i]);
			strs[i] = 0;
			i++;
		}
	}
	if (freeptr)
		free(strs);
	return (NULL);
}

int	ft_count_words(const char *str, char c)
{
	int	i;
	int	newword;

	i = 0;
	newword = 0;
	while (*str)
	{
		if (*str != c && newword == 0)
		{
			newword = 1;
			i++;
		}
		else if (*str == c)
			newword = 0;
		str++;
	}
	return (i);
}

char	*ft_worddup(const char *str, int start, int end)
{
	char	*word;
	int		i;

	i = 0;
	word = malloc((end - start + 1) * sizeof(char));
	while (start < end)
		word[i++] = str[start++];
	word[i] = '\0';
	return (word);
}

char	**ft_split(char const *s, char c)
{
	size_t	i;
	int		word;
	int		j;
	char	**words;

	if (s)
		words = malloc((ft_count_words(s, c) + 1) * sizeof(char *));
	if (!words)
		return (0);
	i = 0;
	word = 0;
	j = -1;
	while (i <= ft_strlen(s))
	{
		if (s[i] != c && j < 0)
			j = i;
		else if ((s[i] == c || i == ft_strlen(s)) && j >= 0)
		{
			words[word++] = ft_worddup(s, j, i);
			j = -1;
		}
		i++;
	}
	words[word] = 0;
	return (words);
}
