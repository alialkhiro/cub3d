/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/02 14:18:36 by algeorge          #+#    #+#             */
/*   Updated: 2022/05/02 14:25:27 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*cur;

	if (n == 0)
		return (s);
	cur = (unsigned char *)s;
	while (n--)
	{
		*cur = (unsigned char)c;
		if (n)
			cur++;
	}
	return (s);
}
