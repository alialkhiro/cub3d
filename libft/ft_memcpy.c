/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/02 15:05:10 by algeorge          #+#    #+#             */
/*   Updated: 2023/03/15 10:20:54 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	char	*dest_cur;
	char	*src_cur;

	if (n == 0 || dest == src)
		return (dest);
	dest_cur = (char *)dest;
	src_cur = (char *)src;
	while (--n)
		*dest_cur++ = *src_cur++;
	*dest_cur = *src_cur;
	return (dest);
}
