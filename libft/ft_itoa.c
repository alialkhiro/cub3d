/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/03 17:22:56 by algeorge          #+#    #+#             */
/*   Updated: 2023/03/17 13:39:30 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_numtoa(char *alpha, unsigned int number, long int len)
{
	while (number > 0)
	{
		alpha[len--] = '0' + (number % 10);
		number = number / 10;
	}
	return (alpha);
}

long int	ft_numlen(int n)
{
	int	len;

	len = 0;
	if (n <= 0)
		len = 1;
	while (n != 0)
	{
		len++;
		n = n / 10;
	}
	return (len);
}

char	*ft_itoa(int n)
{
	char			*alpha;
	long int		len;
	unsigned int	number;

	len = ft_numlen(n);
	alpha = (char *)malloc(sizeof(char) * (len + 1));
	if (!(alpha))
		return (NULL);
	alpha[len--] = '\0';
	if (n == 0)
		alpha[0] = '0';
	if (n < 0)
	{
		number = n * -1;
		alpha[0] = '-';
	}
	else
		number = n;
	alpha = ft_numtoa(alpha, number, len);
	return (alpha);
}
