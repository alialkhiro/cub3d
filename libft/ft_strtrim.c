/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/03 11:37:55 by algeorge          #+#    #+#             */
/*   Updated: 2023/03/17 13:43:40 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	isinset(char c, char const *set)
{
	while (*set)
	{
		if (c == *set++)
			return (1);
	}
	return (0);
}

int	nb_trim(char const *s1, char const *set)
{
	int	n;

	n = 0;
	while (*s1)
	{
		if (isinset(*s1++, set))
			n++;
	}
	return (n);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*new;
	size_t	s1len;
	int		i;

	if (!s1)
		return (NULL);
	if (!set)
		return (ft_strdup(s1));
	s1len = ft_strlen(s1);
	new = (char *)malloc(sizeof(char) * (s1len - nb_trim(s1, set) + 1));
	if (!new)
		return (NULL);
	i = 0;
	while (*s1)
	{
		if (!isinset(*s1, set))
			new[i++] = *s1;
		s1++;
	}
	new[i] = '\0';
	return (new);
}
