/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel_middle.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 14:33:52 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/27 14:33:52 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_lstdel_notfirst(t_list *temp, void *content, void (*del)(void*))
{
	t_list	*previous;
	t_list	*next;

	previous = temp;
	temp = temp->next;
	while (temp)
	{
		next = temp->next;
		if (temp->content == content)
		{
			ft_lstdelone(temp, del);
			previous->next = next;
			break ;
		}
		previous = temp;
		temp = temp->next;
	}
}

void	ft_lstdel_middle(t_list **lst, void *content, void (*del)(void*))
{
	t_list	*temp;
	t_list	*next;

	if (!lst || !*lst)
		return ;
	temp = *lst;
	next = temp->next;
	if (temp->content == content)
	{
		ft_lstdelone(temp, del);
		*lst = next;
	}
	else
		ft_lstdel_notfirst(temp, content, del);
}
