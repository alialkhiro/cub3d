/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/03 10:51:24 by algeorge          #+#    #+#             */
/*   Updated: 2023/03/17 13:43:47 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*comb;
	size_t	s1len;
	size_t	s2len;
	int		i;

	if (!s1 || !s2)
		return (NULL);
	s1len = ft_strlen(s1);
	s2len = ft_strlen(s2);
	comb = (char *)malloc(sizeof(char) * (s1len + s2len + 1));
	if (!comb)
		return (NULL);
	i = 0;
	while (*s1)
	{
		comb[i] = *s1++;
		i++;
	}
	while (*s2)
	{
		comb[i] = *s2++;
		i++;
	}
	comb[i] = '\0';
	return (comb);
}
