/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/02 15:26:44 by algeorge          #+#    #+#             */
/*   Updated: 2022/05/02 15:38:20 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	char	*dest_cur;
	char	*src_cur;
	size_t	i;

	dest_cur = (char *)dest;
	src_cur = (char *)src;
	i = 0;
	if (dest_cur > src_cur)
		while (n-- > 0)
			dest_cur[n] = src_cur[n];
	else
	{
		while (i < n)
		{
			dest_cur[i] = src_cur[i];
			i++;
		}
	}
	return (dest);
}
