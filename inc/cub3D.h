/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3D.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/28 11:30:49 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/07/05 09:43:27 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUBE3D_H

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "libft.h"

# define rgb_limit	255
# define NORTH		0
# define EAST		1
# define SOUTH		2
# define WEST		3
# define BUFF_SIZE	1000

typedef struct s_reader
{
	int		fd;
	char	*buffer;
	char	*data;
	char	*temp;
	int		read_count;
}	t_reader;

typedef struct s_map
{
	char	**data;
	char	**map;
	int		player_x;
	int		player_y;
	char	player_dir;
	char	*north;
	char	*south;
	char	*east;
	char	*west;
	int		ceiling[3];
	int		floor[3];
}	t_map;

char	*ft_read_file(char *file);
char	**data_split(char *data, char splitter);
char	**data_split(char *data, char splitter);
int		map_analyze(t_map *map);
int		texture_analyze(t_map *map, int *line);
int		error_map(char *err_msg, int line);
void	map_clear(void *map);
void	check_input(char **argv, int argc);
void	clear_space(char **str);
void	error_handler(char *err_msg, void *arg, void (*func)(void *));

#endif