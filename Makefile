# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/18 16:38:51 by algeorge          #+#    #+#              #
#    Updated: 2022/10/18 16:38:51 by algeorge         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= cube3d

SRCF	= 	map_loader.c \
			file_reader.c \
			map_analyze.c \
			texture_analyze.c \
			clear_space.c \
			data_split.c

SRCS_DIR	= src
SRCS	= $(addprefix $(SRCS_DIR)/, $(SRCF))
OBJS	= $(SRCS:.c=.o)

CC				= gcc
HEADER			= -I ./inc $(LIBFT_INCL)
CFLAGS			= -Wall -Wextra -Werror -g $(HEADER)

LIBFT_PATH		= ./libft
LIBFT_MAKE		= make -C $(LIBFT_PATH) --no-print-directory
LIBFT_INCL		= -I $(LIBFT_PATH)
LIBFT			= $(LIBFT_PATH)/libft.a

all:	libftmake
		make $(NAME) --no-print-directory

libftmake:
			$(LIBFT_MAKE)


$(NAME):	$(OBJS)
			$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LIBFT)

clean:
				make clean -C ./libft
				rm -rf $(OBJS)

fclean:			clean
				make fclean -C ./libft
				rm -rf $(NAME)

re:				fclean all